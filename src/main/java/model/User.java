package model;

public class User {

    private long id;
    private String username;
    private String password;

    /**
     * constructs a user from the given params.
     * @param username
     * @param password
     */
    public User(String username, String password){
        this.username=username;
        this.password=password;
    }

    /**
     * constructs a user from the given params.
     * @param id
     * @param username
     * @param password
     */
    public User(long id, String username, String password){
        this.id=id;
        this.username=username;
        this.password=password;
    }

    // @return id
    public long getId(){
        return id;
    }

    // @return username
    public String getUsername(){
        return username;
    }

    // @return password
    public String getPassword(){
        return password;
    }

    // @param new username
    public void setUsername(String username) {
        this.username = username;
    }

    // @param new password
    public void setPassword(String password) {
        this.password = password;
    }

    // @return string-representation of the User
    public String toString(){
        return "User"+"\nID:"+ this.id+ "\nUsername: "+ this.getUsername()+"\nPassword"+ this.getPassword()+"\n";
    }
}

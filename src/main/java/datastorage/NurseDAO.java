package datastorage;

import model.Nurse;
import utils.DateConverter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Implements the Interface <code>DAOImp</code>. Overrides methods to generate specific nurse-SQL-queries.
 */
public class NurseDAO extends DAOimp<Nurse>{

    /**
     * constructs Onbject. Calls the Constructor from <code>DAOImp</code> to store the connection.
     * @param conn
     */
    public NurseDAO(Connection conn) {
        super(conn);
    }

    /**
     * generates a <code>INSERT INTO</code>-Statement for a given nurse
     * @param nurse for which a specific INSERT INTO is to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getCreateStatementString(Nurse nurse){
        return String.format("INSERT INTO nurse (firstname, surname, dateOfBirth, telephone) VALUES ('%s', '%s', '%s', '%s')",
                nurse.getFirstName(), nurse.getSurname(), nurse.getDateOfBirth(), nurse.getTelephone());
    }

    /**
     * generates a <code>select</code>-Statement for a given key
     * @param key for which a specific SELECTis to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getReadByIDStatementString(int key) {
        return String.format("SELECT * FROM nurse WHERE nid = %d", key);
    }

    /**
     * maps a <code>ResultSet</code> to a <code>Nurse</code>
     * @param result ResultSet with a single row. Columns will be mapped to a nurse-object.
     * @return nurse with the data from the resultSet.
     */
    @Override
    protected Nurse getInstanceFromResultSet(ResultSet result) throws SQLException {
        Nurse n = null;
        LocalDate date = DateConverter.convertStringToLocalDate(result.getString(4));
        n = new Nurse(result.getInt(1), result.getString(2),
                result.getString(3), date, result.getString(5));
        return n;
    }

    /**
     * generates a <code>SELECT</code>-Statement for all nurses.
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getReadAllStatementString() {
        return "SELECT * FROM nurse";
    }

    /**
     * maps a <code>ResultSet</code> to a <code>Nurse-List</code>
     * @param result ResultSet with a multiple rows. Data will be mapped to nurse-object.
     * @return ArrayList with nurses from the resultSet.
     */
    @Override
    protected ArrayList<Nurse> getListFromResultSet(ResultSet result) throws SQLException {
        ArrayList<Nurse> list = new ArrayList<Nurse>();
        Nurse n = null;
        while (result.next()) {
            LocalDate date = DateConverter.convertStringToLocalDate(result.getString(4));
            n = new Nurse(result.getInt(1), result.getString(2),
                    result.getString(3), date, result.getString(5));
            list.add(n);
        }
        return list;
    }

    /**
     * generates a <code>UPDATE</code>-Statement for a given nurse
     * @param nurse for which a specific update is to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getUpdateStatementString(Nurse nurse) {
        return String.format("UPDATE nurse SET firstname = '%s', surname = '%s', dateOfBirth = '%s', telephone = '%s' WHERE nid = %d",
                nurse.getFirstName(), nurse.getSurname(), nurse.getDateOfBirth(), nurse.getTelephone(), nurse.getNid());
    }

    /**
     * generates a <code>delete</code>-Statement for a given key
     * @param key for which a specific DELETE is to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getDeleteStatementString(int key) {
        return String.format("Delete FROM nurse WHERE nid=%d", key);
    }
}

# NHPlus

##Informationen zur Lernsituation
Du bist Mitarbeiter der HiTec GmbH, die seit über 15 Jahren IT-Dienstleister und seit einigen Jahren ISO/IEC 27001 zertifiziert ist. Die HiTec GmbH ist ein mittelgroßes IT-Systemhaus und ist auf dem IT-Markt mit folgenden Dienstleistungen und Produkten vetreten: 

Entwicklung: Erstellung eigener Softwareprodukte

Consulting: Anwenderberatung und Schulungen zu neuen IT- und Kommunikationstechnologien , Applikationen und IT-Sicherheit

IT-Systembereich: Lieferung und Verkauf einzelner IT-Komponenten bis zur Planung und Installation komplexer Netzwerke und Dienste

Support und Wartung: Betreuung von einfachen und vernetzten IT-Systemen (Hard- und Software)

Für jede Dienstleistung gibt es Abteilungen mit spezialisierten Mitarbeitern. Jede Abteilung hat einen Abteilungs- bzw. Projektleiter, der wiederum eng mit den anderen Abteilungsleitern zusammenarbeitet.

 

##Projektumfeld und Projektdefinition

Du arbeitest als Softwareentwickler in der Entwicklungsabteilung. Aktuell bist du dem Team zugeordnet, das das Projekt "NHPlus" betreut. Dessen Auftraggeber - das Betreuungs- und Pflegeheim "Curanum Schwachhausen" - ist ein Pflegeheim im Bremer Stadteil Schwachhausen - bietet seinen in eigenen Zimmern untergebrachten Bewohnern umfangreiche Therapie- und Serviceleistungen an, damit diese so lange wie möglich selbstbestimmt und unabhängig im Pflegeheim wohnen können. Curanum Schwachhausen hat bei der HiTec GmbH eine Individualsoftware zur Verwaltung der Patienten und den an ihnen durchgeführten Behandlungen in Auftrag gegeben. Aktuell werden die Behandlungen direkt nach ihrer Durchführung durch die entsprechende Pflegekraft handschriftlich auf einem Vordruck erfasst und in einem Monatsordner abgelegt. Diese Vorgehensweise führt dazu, dass Auswertungen wie z.B. welche Behandlungen ein Patient erhalten oder welche Pflegkraft eine bestimmte Behandlung durchgeführt hat, einen hohen Arbeitsaufwand nach sich ziehen. Durch NHPlus soll die Verwaltung der Patienten und ihrer Behandlungen elektronisch abgebildet und auf diese Weise vereinfacht werden.

Bei den bisher stattgefundenen Meetings mit dem Kunden konnten folgende Anforderungen an NHPlus identifiziert werden:

- alle Patienten sollen mit ihrem vollen Namen, Geburtstag, Pflegegrad, dem Raum, in dem sie im Heim untergebracht sind, sowie ihrem Vermögensstand erfasst werden.

- Die Pflegekräfte werden mit ihrem vollen Namen und ihrer Telefonnumer erfasst, um sie auf Station schnell erreichen zu können.

- jede Pflegekraft erfasst eine Behandlung elektronisch, indem sie den Patienten, das Datum, den Beginn, das Ende, die Behandlungsart sowie einen längeren Text zur Behandlung erfasst.

- Die Software muss den Anforderungen des Datenschutzes entsprechen. 

- Die Software ist zunächst als Desktopanwendung zu entwickeln, da die Pflegekräfte ihre Behandlungen an einem stationären Rechner in ihrem Aufenthaltsraum erfassen sollen.

 

Da in der Entwicklungsabteilung der HiTech GmbH agile Vorgehensweisen vorgeschrieben sind, wurde für NHPlus Scum als Vorgehensweise gewählt.

 

##Stand des Projektes

Die Anwendung wurde um Pflegekräfte erweitert. Es können nun Pflegekräfte angelegt und verwaltet werden. Zusätzlich hierzu können neuen Behandlungen Pflegekräfte zugewiesen werden. 
Es wurde ein Anmeldescreen erstellt, um unbefugten Personen den Zugang zur Anwendung zu verweigern.
Der Parameter 'Vermögensstand' wurde entfernt.
Daten können durch das anklicken der Datensätze und ein darauffolgendes klicken des 'Sperren' Knopfes gesperrt werden. Dabei werden die Datensätze nicht gelöscht, jedoch werden diese nicht mehr angezeigt. Nach einer Speicherfrist von 10 Jahren werden diese automatisch gelöscht.

## Hinweise zum Anmeldescreen
- User 1: Habib  
- Passwort1: 1994
- User 2: Berkan    
- Passwort2: 1995

## Testfälle
Folgende Testfälle wurden erfolgreich durchgeführt:
1. Einloggen mit falschen User Namen und richtigen Passwort -> Programm verhaltet wie erwartet.
2. Einloggen mit richtigen User Namen und falschen Passwort -> Programm verhaltet wie erwartet.
3. Einloggen mit User Namen von einem User und Passwort voneinander -> Programm verhaltet wie erwartet.
4. In User Name Feld Zeichnen eingetragen -> Programm verhaltet wie erwartet.
5. Nach mehrere Versuche mit falschen Login Daten einzuloggen, die richtigen Login Daten benutzt und erfolgreich eingeloggt.
6. Erstellen eines neuen Patienten führt zu einer Ergänzung der ComboBox im Behandlungsbereich.
7. Erstellen einer neuen Pflegekraft führt zu einer Ergänzung der ComboBox beim Anlegen einer neuen Behandlung.
8. Das Ablaufen der 10-Jahres Frist führt zu einer Löschung der Behandlungsdatensätze.
9. Das Erstellen von Behandlungen vor dem Jahr 2010 werden nach einer Aktualisierung gelöscht.

Folgendes führt zu Fehlern:
1. Falsches Format des Datums (statt yyyy-MM-dd --> dd-MM-yyyy)

## Technische Hinweise zur Datenbank

- Benutzername: SA
- Passwort: SA
- Bitte nicht in die Datenbank schauen, während die Applikation läuft. Das sorgt leider für einen Lock, der erst wieder verschwindet, wenn IntelliJ neugestartet wird!